﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagController : MonoBehaviour
{
    [SerializeField] private bool FollowPlayer;
    [SerializeField] private Transform PlayerLeftHand;

    // Start is called before the first frame update
    void Start()
    {
        FollowPlayer = false;
        PlayerLeftHand = GameObject.Find("LeftHand").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (FollowPlayer)
        {
            transform.position = PlayerLeftHand.position;
        }
    }
    public void followPlayer()
    {
        FollowPlayer = true;
    }
    public bool getFollowPlayer()
    {
        return FollowPlayer;
    }
}
