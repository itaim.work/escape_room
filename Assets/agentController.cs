﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class agentController : MonoBehaviour
{
    [SerializeField] private Transform Player;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private float Counter, CounterReset;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {

        if (agent.remainingDistance <= 2f && Counter <= 0)
        {
            Player.gameObject.GetComponent<PlayerMovement>().TakeDamage(10);
            Counter = CounterReset;
        }
        Counter -= Time.deltaTime;
    }


}
