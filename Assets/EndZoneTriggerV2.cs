﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndZoneTriggerV2 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (GameObject.Find("Flag").GetComponent<FlagController>().getFollowPlayer())
            {
                print("Game Won!");
            }
        }
    }
}
