﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target2 : MonoBehaviour
{
    public static bool isTarget2Hit = false; //  a static bool for the isthetargethit


    private void Start()
    {
        isTarget2Hit = false;
    }
    /// <summary>
    /// IEnumerator is a delay function in unity, when called amd given a timer, it will count down that timer and after the time runds out, calls the rest of the function
    /// </summary>
    /// <returns></returns>
    IEnumerator ResetTargetTimer()
    {
        yield return new WaitForSeconds(20); // call the IEnumerator to count down 10 seconds 
        isTarget2Hit = false; // set the target is hit bool to false
    } 

    private void OnTriggerEnter(Collider other) // if an object get into the trigger zone
    {
        isTarget2Hit = true; // change the istargethit to true
        StartCoroutine(ResetTargetTimer()); // calls the coroutine "Reset Target Timer"

    }

}
