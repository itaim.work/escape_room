﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{

    /// <summary>
    /// play game function
    /// </summary>
    public void PlayGame()
    {
        SceneManager.LoadScene(1); // calls the scene manager to load 
    }

    /// <summary>
    /// close game function
    /// </summary>
    public void CloseGame()
    {
        Application.Quit(); // call the application to close itself
    }
}
