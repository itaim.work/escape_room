﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public SceneController SceneControllerScript; // scene controller script var

    public CharacterController controller; //  the character controller var
    public float Speed = 12f; // player speed var

    public Transform GroundCheck; // A GroundCheck transform var for the gravity calculations
    public LayerMask GroundLayerMask; // a layermask var for the gravity calculations
    public float GroundCheckDistance = 0.4f; // the radios of the ground check
    public bool IsGrounded; //  set to true if the playe is on the ground

    public float Gravity = -9.81f; // gravity var
    public float JumpForce = 5f; //  jump force var
    Vector3 velocity; // a velocity vector 3 var
    [SerializeField] private int HP = 100;


    // Update is called once per frame
    void Update()
    {
        if(transform.position.y <= -10) // if the player y position value is below -10
        {
            SceneControllerScript.gameLose(); // call the scene manager to inform him that the player lost
        }

        IsGrounded = Physics.CheckSphere(GroundCheck.position, GroundCheckDistance, GroundLayerMask); // a check to see if the player is on the ground via creating a sphere and checking for the ground woth the sphere

        velocity.y += Gravity * Time.deltaTime; // apllies gravity to the player

        float x = Input.GetAxis("Horizontal"); //  horizontal input

        float z = Input.GetAxis("Vertical"); //  vertical input

        Vector3 move = transform.right * x + transform.forward * z; //  setting the new move directon

        controller.Move(move * Speed * Time.deltaTime); // moveing the player on the ground

        if (Input.GetButton("Jump") && IsGrounded) // if the user press "space" and the player is on the ground
        {
            velocity.y = Mathf.Sqrt(JumpForce * 2f - Gravity); //  sets the y axis velocity of the player
        }
        controller.Move(velocity * Time.deltaTime); // moving the player up in the air

        if (HP <= 0)
        {
            //lose
        }
    }

    public void TakeDamage(int value)
    {
        HP -= value;
    }
}
