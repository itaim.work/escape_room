﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndZoneTrigger : MonoBehaviour
{
    public SceneController SceneControllerScript; // scene controller script var

    /// <summary>
    /// on trigger enter is a function, that can be called when two objects, won with a collider and a rigidbody components and one with a collider that is active as a trigger collider. 
    /// when the two objects are colliding, the function can be called in the one with the trigger collider
    /// and give the other object information.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) // if the object that collided with the trigger zone is the player
        {
            SceneControllerScript.GameWin(); // send a ping to the scene controller to infrom him that the player won
        }
    }

}
