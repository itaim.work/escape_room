﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1_Manager : MonoBehaviour
{

    public bool Level_1_Complete = false; // set to true if the level is complete
    public bool BluePressed = false; // set to true if the player pressed the blue button, and false if not
    public bool RedPressed = false; // set to true if the player pressed the red button, and false if not
    public bool YellowPressed = false; //  set to true if the player pressed the yellow button, and false if not
    public Transform Level_1_Door; // a transform var for the door
    bool IsTheDoorOpening = false; // if the door is opening = true
    bool HasTheDoorBeenOpend = false; // set to true if the door has been opened
    public GameObject Level_2_Manager;


    // Update is called once per frame
    void Update()
    {
        if (Selector.GetHit().transform.tag == ("Blue Button")) // if the player pressed the blue button
        {
            BluePressed = true; // if true set the BluePressed bool to true
        }
        if (BluePressed && Selector.GetHit().transform.CompareTag("Red Button")) // if bluePressed is true and the player  pressed the red button
        {
            RedPressed = true; // if true set RedPressed to true
        }
        if (!RedPressed && BluePressed && Selector.GetHit().transform.CompareTag("Yellow button")) // if bluePressed is true and RedPressed is false and the player pressed the yellow button
        {
            BluePressed = false; // setting BluePressed to false
        }
        if (BluePressed && RedPressed && Selector.GetHit().transform.CompareTag("Yellow button")) // if redPressed is true and BluePressed is true and the player pressed the yellow button
        {
            YellowPressed = true; // setting the YellowPressed to true
            Level_1_Complete = true; // setting the level_1_complete to true
        }
        if (Level_1_Complete) // if level one is complete
        {
            Door(); // calls the door function
            StartCoroutine(DestroyLevel()); //  calls the coroutine "Destroy level"

        }
    }

    void Door()
    {
        Level_1_Door.transform.Translate(Vector3.up * Time.deltaTime * 4); // moving the player up

        if (Level_1_Door.transform.position.y > 4) // if the player arrived to a certin hieght
        {
            Invoke("DoorToClose", 1); // calling the door closing function with 1 sec delay
            IsTheDoorOpening = false; // after the invoke set the door is opening to false
        }
        if (IsTheDoorOpening == false && HasTheDoorBeenOpend) // after the DoorToClose function ran
        {
            Level_1_Door.transform.Translate(Vector3.down * Time.deltaTime * 4); // stopping the door
        }
    }
    void DoorToClose()
    {
        HasTheDoorBeenOpend = true;
    }
    /// <summary>
    /// IEnumerator is a delay function in unity, when called amd given a timer, it will count down that timer and after the time runds out, calls the rest of the function
    /// </summary>
    /// <returns></returns>
    IEnumerator DestroyLevel()
    {

        yield return new WaitForSeconds(5); // call the IEnumerator to count down 5 seconds
        Level_2_Manager.SetActive(true); // activates the second level
        Destroy(gameObject); // turns off the first level

    }
}
