﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_3_Manager : MonoBehaviour
{
    RaycastHit HitInfo;

    public GameObject PathWay;

    // Update is called once per frame
    void Update()
    {
        HitInfo = Selector.GetHit();

        if(GameObject.FindGameObjectWithTag("Level 3 target") != null && HitInfo.transform.CompareTag("Level 3 target"))
        {
            Destroy(HitInfo.transform.gameObject);
        }

        if(GameObject.FindGameObjectWithTag("Level 3 target") == null)
        {
            PathWay.SetActive(true);
        }
    }
}
