﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{

    public static RaycastHit Hit; // a raycasthit var

    private void Start()
    {
        Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out Hit, Mathf.Infinity); // shoots a random ray to make sure the Hit var isnt empty
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // if the user presses the mouse 1 button (left click)
        {
            Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out Hit, Mathf.Infinity); // shoot a ray infront of the player.
        }
    }
    public static RaycastHit GetHit()
    {
        return Hit; // return the Hit info
    }
}
