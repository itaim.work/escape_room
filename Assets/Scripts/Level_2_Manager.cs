﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_2_Manager : MonoBehaviour
{

    public bool IsTargetHit1 = false; // bool IsTarget1Hit
    public bool IsTargetHit2 = false; // bool IsTarget2Hit
    public bool IsTargetHit3 = false; // bool IsTarget3Hit
    public bool IsTargetHit4 = false; // bool IsTarget4Hit
    public bool Level_2_Complete = false; // bool is level 2 complete
    bool IsTheDoorOpening; // bool is the door opening for the door open action
    bool HasTheDoorBeenOpend; // bool has the door been opened for the door stooping action

    public Transform Door_2; // the door tramsform var

    private void Start()
    {
        Level_2_Complete = false; // set level_2_complete to false

    }

    // Update is called once per frame
    void Update()
    {
        /*
         * to check if the player has enterd each one of the targets.
         * we call the targets scripts and checking if the IsTarget-X-Hit bool of the targets script has changed (x = 1-4)
         */
        IsTargetHit1 = target1.isTarget1Hit; 
        IsTargetHit2 = target2.isTarget2Hit;
        IsTargetHit3 = Target3.isTarget3Hit;
        IsTargetHit4 = Target4.isTarget4Hit;

        if(IsTargetHit1 && IsTargetHit2 && IsTargetHit3 && IsTargetHit4) // if the player has reached all of the targets
        {
            Level_2_Complete = true; // set level_2_complete to true
        }

        if (Level_2_Complete)
        {
            Door(); // calls the door function

        }
    }
    void Door()
    {
        Door_2.transform.Translate(Vector3.up * Time.deltaTime * 4); // moving the player up

        if (Door_2.transform.position.y > 4) // if the player arrived to a certin hieght
        {
            Invoke("DoorToClose", 1); // calling the door closing function with 1 sec delay
            IsTheDoorOpening = false; // after the invoke set the door is opening to false
        }
        if (IsTheDoorOpening == false && HasTheDoorBeenOpend) // after the DoorToClose function ran
        {
            Door_2.transform.Translate(Vector3.down * Time.deltaTime * 4); // stopping the door
        }
    }
    void DoorToClose()
    {
        HasTheDoorBeenOpend = true;
    }

}
