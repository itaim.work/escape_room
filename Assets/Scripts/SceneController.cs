﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public GameObject PauseMenuUI; // pause menu U I var
    public GameObject LoseMenuUI; // lose menu UI var
    public GameObject WinMenuUI; // win menu UI var

    private void Start()
    {
        Time.timeScale = 1;
        Cursor.visible = false; 
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKey(KeyCode.P)) // if the user presses the "P" key
        {
            PauseGame(); // run the pause game function
        }
        if(Input.GetKey(KeyCode.R)) // if the user presses the "R" key
        {
            ResumeGame(); // run the resume game function
        }

    }

    /// <summary>
    /// pause game function
    /// </summary>
    public void PauseGame()
    {
        Cursor.lockState = CursorLockMode.None; // unlock the curser to the center of the screen
        Time.timeScale = 0; // set the time scale to 0 to stop the game cycle
        Cursor.visible = true;

        PauseMenuUI.SetActive(true); // open the pause menu UI 
    }

    /// <summary>
    /// resume game function
    /// </summary>
    public void ResumeGame()
    {
        Time.timeScale = 1; // set the time scale to 1 to resume the game cycle
        Cursor.lockState = CursorLockMode.Locked; // lock the curser to the center of the screen
        Cursor.visible = false;
        PauseMenuUI.SetActive(false); // close the pause menu UI

    }

    /// <summary>
    /// restart game function
    /// </summary>
    public void RestartGame()
    {
        Cursor.visible = false;
        Time.timeScale = 1; // set the time scale to 1 to resume the game cycle 
        Cursor.lockState = CursorLockMode.Locked; // lock the curser to the center of the screen
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // call the scene manager to reload the scene

    }

    /// <summary>
    /// open main Menu function
    /// </summary>
    public void MainMenu()
    {
        Time.timeScale = 1;  // set the time scale to 1 to resume the game cycle 
        SceneManager.LoadScene(0); // call the scene manager to load the first scene which is the main menu scene
    }

    /// <summary>
    /// game win function
    /// </summary>
    public void GameWin()
    {
        Cursor.lockState = CursorLockMode.None; // unlock the curser to the center of the screen
        Time.timeScale = 0; // set the time scale to 0 to stop the game cycle 
        Cursor.visible = true;

        WinMenuUI.SetActive(true); // open the win menu UI
    }

    /// <summary>
    /// game lose function
    /// </summary>
    public void gameLose()
    {
        Cursor.lockState = CursorLockMode.None; // unlock the curser to the center of the screen
        Time.timeScale = 0; // set the time scale to 0 to stop the game cycle 
        Cursor.visible = true;

        LoseMenuUI.SetActive(true); // open the lose menu UI
    }
}
