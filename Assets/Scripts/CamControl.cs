﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    public float Sens = 100f; // mouse sensitivity

    public Transform PlayerBody; // the player body for the rotation

   public float Xrotation = 0f; // the camera rotation var(variable)

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // looking the mouse to the middle of the screen
    }

    // Update is called once per frame
    void Update()
    {
        float MouseX = Input.GetAxis("Mouse X") * Sens * Time.deltaTime; // an axis input from the mouse x movement

        float MouseY = Input.GetAxis("Mouse Y") * Sens * Time.deltaTime; // an axis input from the mouse y movement

        Xrotation -= MouseY; // to prevent clamping

        Xrotation = Mathf.Clamp(Xrotation, -90f, 90f); //  calling a math command, to "clamp" the Xroation to -90-90 degrees view point

        transform.localRotation = Quaternion.Euler(Xrotation, 0f, 0f); // sets the camera rotation

        PlayerBody.Rotate(Vector3.up * MouseX);
    }
}
