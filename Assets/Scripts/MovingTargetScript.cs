﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTargetScript : MonoBehaviour
{
    public bool NeedNewDirection; // if set to true then need new direction

    public bool MoveLeft; // if set to true move the target left, and if false move right

    public float speed = 10f; // the target movement speed var

    // Update is called once per frame
    void Update()
    {
        if(Random.Range(1,3) == 1 && NeedNewDirection) // random.range(1,3) give a 50 50 chance, and the need new direction needs to be true for this to run
        {
            MoveLeft = true; // 50 % chane to go left

            NeedNewDirection = false; // set need new direction to false
        }
        else if( NeedNewDirection) 
        {
            MoveLeft = false; // 50 % chance to go right

            NeedNewDirection = false; // set need new direction to false
        }

        if(transform.position.z <= 0 || transform.position.z >= 15) // if the target arrived at each one of its edges
        {
            NeedNewDirection = true; // need new direction set to true
        }

        if(MoveLeft) // if need to move left
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 0f), speed * Time.deltaTime); // move the target left
        }
        else // if need to move right
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 15f), speed * Time.deltaTime); // move the target right

        }
    }
}
