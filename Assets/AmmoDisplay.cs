﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoDisplay : MonoBehaviour
{
    [SerializeField] private int magCounter, overAllCount = 120;
    [SerializeField] private Text AmmoDisplayText;

    // Start is called before the first frame update
    void Start()
    {
        magCounter = 30;
        overAllCount = 90;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            magCounter--;
        }

        AmmoDisplayText.text = overAllCount + " /  " + magCounter; 
    }
}
